package com.com470.Combinatoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CombinatoriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CombinatoriaApplication.class, args);
	}

}
