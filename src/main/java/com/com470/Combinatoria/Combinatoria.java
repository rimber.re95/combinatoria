
package com.com470.Combinatoria;


public class Combinatoria {
    public int factorial(int n){
        if(n < 0){return 0;
        }else{
            if(n > 1){ return n*factorial(n-1);   }
        } return 1;
    }
    public String getCombinatoria(int x ,int n){
        
        if(x>=0){
            if(n>=0){
                if(x<=n){
                    if(n!=x){
                        int factorialn=factorial(n);
                        int factorialx=factorial(x);
                        int nx = n-x;
                        int factorialnx=factorial(nx);

                        int resultado = factorialn/(factorialx*factorialnx);

                        return resultado+"";
                    }else return "1";
             }else return "Error:n debe ser mayor que x";
            }else {
                return "Error:n debe ser positivo";
            }

        }else{ 
            return "Error:x debe ser positivo";
        }
        
    }
     public String getCombinatoriaCadena(String X ,String N){
        int x=Integer.parseInt(X);
        int n=Integer.parseInt(N);
        if(x>=0){
            if(n>=0){
                if(x<=n){
                    if(n!=x){
                        int factorialn=factorial(n);
                        int factorialx=factorial(x);
                        int nx = n-x;
                        int factorialnx=factorial(nx);

                        int resultado = factorialn/(factorialx*factorialnx);

                        return resultado+"";
                    }else return "1";
             }else return "Error:n debe ser mayor que x";
            }else {
                return "Error:n debe ser positivo";
            }

        }else{ 
            return "Error:x debe ser positivo";
        }
        
    }
}

