package com.com470.Combinatoria;

import com.com470.Combinatoria.Combinatoria;
import org.junit.Test;
import static org.junit.Assert.*;

public class CombinatoriaTest {
    private Combinatoria combinatoria=new Combinatoria();
    public CombinatoriaTest() {
    }

    @Test
    public void factorial(){
        int factorial=combinatoria.factorial(5);
        assertEquals(factorial,120);
    }
    @Test
    public void xMayorQuen(){
        String result=combinatoria.getCombinatoria(8, 2);
        assertEquals(result,"Error:n debe ser mayor que x");
    }
    @Test
    public void xNegativo(){
        String result=combinatoria.getCombinatoria(-8, 2);
        assertEquals(result,"Error:x debe ser positivo");
    }
    @Test
    public void nNegativo(){
        String result=combinatoria.getCombinatoria(8, -2);
        assertEquals(result,"Error:n debe ser positivo");
    }
    @Test
    public void x0yn3(){
        String result=combinatoria.getCombinatoria(0, 3);
        assertEquals(result,"1");
    }
    @Test
    public void x1yn4(){
        String result=combinatoria.getCombinatoria(1, 4);
        assertEquals(result,"4");
    }
    @Test
    public void x2yn5(){
        String result=combinatoria.getCombinatoria(2, 5);
        assertEquals(result,"10");
    }
    @Test
    public void c3yn7(){
        String result=combinatoria.getCombinatoria(3, 7);
        assertEquals(result,"35");
    }
    @Test
    public void xIgualQuen(){
        String result=combinatoria.getCombinatoria(2, 2);
        assertEquals(result,"1");
    }

    @Test
    public void valoresConCadena(){
        String result=combinatoria.getCombinatoriaCadena("2", "3");
        assertEquals(result,"3");
    }
    
    @Test
    public void x1yn1(){
        String result=combinatoria.getCombinatoria(1, 1);
        assertEquals(result,"1");
    }
}
